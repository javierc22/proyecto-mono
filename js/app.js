class Interfaz{
  mostrarMensaje(mensaje, tipo){
    const divMensaje = document.createElement('div');

    if( tipo === 'error'){
      divMensaje.classList.add('mensaje', 'alert', 'alert-danger');
    } else {
      divMensaje.classList.add('mensaje', 'correcto');
    }

    divMensaje.innerHTML = `${mensaje}`;
    formulario.insertBefore(divMensaje, document.querySelector('.form-group'));

    setTimeout(() => {
      document.querySelector('.mensaje').remove();
    }, 3000);
  }

  mostrarResultado(value){
    const resultado = document.getElementById('resultado');
    const divContent = document.createElement('div');
    const img0 = document.createElement('img');
    const img1 = document.createElement('img');
    const img2 = document.createElement('img');
    const img3 = document.createElement('img');
    const img4 = document.createElement('img');
    const img5 = document.createElement('img');

    img0.setAttribute('src', `images/${value[0]}.jpg`);
    img0.setAttribute('width', '450');
    img0.classList.add('img-fluid', 'rounded', 'mx-auto', 'd-block');
    divContent.appendChild(img0)
    resultado.appendChild(divContent);

    img1.setAttribute('src', `images/${value[1]}.jpg`);
    img1.setAttribute('width', '450');
    img1.classList.add('img-fluid', 'rounded', 'mx-auto', 'd-block');
    divContent.appendChild(img1)
    resultado.appendChild(divContent);

    img2.setAttribute('src', `images/${value[2]}.jpg`);
    img2.setAttribute('width', '450');
    img2.classList.add('img-fluid', 'rounded', 'mx-auto', 'd-block');
    divContent.appendChild(img2)
    resultado.appendChild(divContent);

    img3.setAttribute('src', `images/${value[3]}.jpg`);
    img3.setAttribute('width', '450');
    img3.classList.add('img-fluid', 'rounded', 'mx-auto', 'd-block');
    divContent.appendChild(img3)
    resultado.appendChild(divContent);

    img4.setAttribute('src', `images/${value[4]}.jpg`);
    img4.setAttribute('width', '450');
    img4.classList.add('img-fluid', 'rounded', 'mx-auto', 'd-block');
    divContent.appendChild(img4)
    resultado.appendChild(divContent);

    img5.setAttribute('src', `images/${value[5]}.jpg`);
    img5.setAttribute('width', '450');
    img5.classList.add('img-fluid', 'rounded', 'mx-auto', 'd-block');
    divContent.appendChild(img5)
    resultado.appendChild(divContent);
  }
}

// ------------ Leyendo Formulario ----------------- //
const formulario = document.getElementById('formulario');

formulario.addEventListener('submit', (e) =>{
  e.preventDefault();

  // Obteniendo valor de expresión seleccionada:
  const expression = document.getElementById('expresionesCotidianas');
  const selectedExpression = expression.value;

  // Instanciando la clase Interfaz:
  const interfaz = new Interfaz();

  // Validando formulario:
  if (selectedExpression === ''){
    // Muestra mensaje de error
    interfaz.mostrarMensaje('Ingresa una expresión', 'error');
  } else {
    // Limpia resultados anteriores
    const resultados = document.querySelector('#resultado div');
    if( resultados != null){
      resultados.remove();
    }
    // Muestra resultados
    interfaz.mostrarResultado(selectedExpression);
  }
});